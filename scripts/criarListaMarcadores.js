import bicicletarios from "./bicicletarios.js"

let ele =  "";
/**
 * Retorna um elemento de lista com os dados do bicicletario
 * @param {Object} locais 
 * @returns HTMLLIELEMENT
 */
export function criarLista(locais){
    ele = document.createElement('div');
    let li = document.createElement('li');
    li.classList.add("list-group-item")

    li.addEventListener("mouseover", ()=>{
        span.style.display = "initial"
    })
    li.addEventListener("mouseout", ()=>{
        span.style.display = "none"
    })
    
    let span = document.createElement('span');
    span.textContent = bicicletarios.records[locais.id][2].slice(4);
    ele.className = "marker"
    span.className = "markerInfo"
    ele.appendChild(span);



    li.innerHTML = `<div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">${bicicletarios.records[locais.id][2].slice(4)}</h5>
        <small>Distância: ${locais.distanciaDaQuery} km</small>
    </div>
    <p class="mb-1">${bicicletarios.records[locais.id][3]}</p>`



    return li;
}
/**
 * Retorna o elemento de div
 * @returns HTMLDIVELEMENT
 */
export function criarCaixaDescricao(){
    return ele;
}



