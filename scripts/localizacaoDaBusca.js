/**
 * Retorna uma lista de objetos de dados geograficos de uma query   
 * @param {string} query 
 * @returns {Object | Array} Lista de objetos JSON com os resultados de busca da API nominatim.
 */
async function localizadorDePosicao(query){
    //a busca com a query acompanhada do estado pernambuco para restringir a busca ao estado do bicicletario
    let url = 'https://nominatim.openstreetmap.org/search?q='+query+'+pernambuco&format=json&countrycodes=br&addressdetails=1';
    var api = await fetch(url)
    .then((response)=>{
        return response.json();
    })
    .then((data)=>{
        return data;
    })

    return api;
}

export default localizadorDePosicao;