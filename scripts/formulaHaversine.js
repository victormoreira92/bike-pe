/**
 * Formula de Haversine para calcular a distancia entre dois pontos
 * 
 * @param {number} qLat 
 * @param {number} qLon 
 * @param {number} bLat 
 * @param {number} bLon 
 * @returns {number} distancia entre duas coordenadas
 */
export default function formulaHaversine(qLat, qLon, bLat, bLon){

    let dLat = decimal_para_radiano(qLat - bLat);
    let dLon = decimal_para_radiano(qLon - bLon);

    let latQ = decimal_para_radiano(qLat);
    let latB = decimal_para_radiano(bLat);

    let a = Math.pow(Math.sin(dLat/2),2) + Math.pow(Math.sin(dLon/2),2)* Math.cos(latQ) * Math.cos(latB);
    
    const rad = 6371;
    let c = 2 * Math.asin(Math.sqrt(a));
    return (rad * c).toFixed(3);

}
/**
 * Calcula o valor de uma coordernada em radiano.
 * @param {number} decimal 
 * @returns {number} Valor da coordernada em radiano
 */
function decimal_para_radiano(decimal)
{
  var pi = Math.PI;
  return decimal * (pi/180);
}

