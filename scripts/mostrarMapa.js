// Objeto da api MapBox que mostra o mapa de Recife
mapboxgl.accessToken = 'pk.eyJ1Ijoidmhtcmo5MiIsImEiOiIzODY4ODczYjlmNWNkMjJkNTNmMzQ5NTFlNzM2NzlhMSJ9.UZTFI7zAF9Mq51T3hFK3UQ';
    const map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v12',
        center: [-34.882719, -8.041951],
        zoom: 12
    });
export {map};



