import localizadorDePosicao from "./localizacaoDaBusca.js";
import bicicletarios  from "./bicicletarios.js";
import formulaHaversine from "./formulaHaversine.js";

/**
 * Calcula a distancia do local de busca e retorna uma lista de objetos
 * ordenada crescente de distancias da query com os pontos de biclceta
 * 
 * @param {string} query 
 * @param {number} limit 
 * @returns {(object | Array)} lista de objetos com a 
 *  
 */
export default async function calcularDistanciaEntreQueryBicicletarios (query, limit){
    let localizacaoQuery =  await localizadorDePosicao(query);
    let listaBicicletarioInfo = [];
    let dadosGeograficos = {};

    bicicletarios.records.map((bicicletario,index)=>{

        //Objeto com as info dos pontos de bicicletario
        let bicicletarioInfo = {    
            id: index,
            distanciaDaQuery: formulaHaversine(localizacaoQuery[0].lat,
            localizacaoQuery[0].lon,bicicletario[6],bicicletario[7]),
            lat: bicicletario[6],
            lon:bicicletario[7],
        };
        if(bicicletarioInfo.distanciaDaQuery < 10){
            listaBicicletarioInfo.push(bicicletarioInfo);
        }

    })

    //Ordenacao cresente da lista pelo ponto de bicicletario e quebrada pelo limite de resultado de busca
        if(listaBicicletarioInfo.length > 0){
            listaBicicletarioInfo = listaBicicletarioInfo.sort((a,b)=>{ return a.distanciaDaQuery - b.distanciaDaQuery;})
                .slice(0,limit);
        
            dadosGeograficos = {
                query:{
                    lat: localizacaoQuery[0].lat,
                    lon: localizacaoQuery[0].lon,
                },
                bicicletarios: listaBicicletarioInfo
            }
        }

        return dadosGeograficos;
        
}
