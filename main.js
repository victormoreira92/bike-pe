import calcularDistanciaEntreQueryBicicletarios from "./scripts/calcularDistancias.js";
import {criarLista, criarCaixaDescricao} from "./scripts/criarListaMarcadores.js"
import { map} from "./scripts/mostrarMapa.js";


var form = document.querySelector('form');
var secaoResultados = document.querySelector('.secaoResultados');
var listaResultados = document.querySelector('.listaResultados');
var btn = document.querySelector('.btn');
map.addControl(new mapboxgl.NavigationControl()); // objeto de Mapa Maker adicionando controles de navegacao
let listaMarcadoresMapa = [];


form.addEventListener('submit',(e)=>{
    e.preventDefault()
    let query = e.target[0].value;
    let limit = e.target[1].value;

    //Limpar o form, secao de resultado e os marcadores no mapa
    form.reset();
    secaoResultados.classList.add("invisible");
    listaResultados.innerHTML = "";
    listaMarcadoresMapa.forEach((marker)=>{marker.remove()})

    
    //Verifica se a query está vazia
    if(query){
        btn.querySelector('.spinner').classList.remove('d-none');
        btn.querySelector('.textoBtn').textContent = 'Carregando';
        
        var listaMarcadoresMapaBicicletario = calcularDistanciaEntreQueryBicicletarios(query, limit); 
        listaMarcadoresMapaBicicletario.then((data)=>{
        renderizarLista(data)});


    } else{
        alert("Busca vazia, digite um endereço")
    }

})


/**
 * Renderiza na secao de resultados a lista com os 
 * pontos de bicicleta mais próximos.
 * 
 * @param {} dadosGeograficos 
 */
function renderizarLista(dadosGeograficos){
    if(Object.keys(dadosGeograficos).length > 0){   

        //cria marcador de busca no mapa 
        const marker1 = new mapboxgl.Marker({color:'red'})
                .setLngLat([ dadosGeograficos.query.lon, dadosGeograficos.query.lat])
                .addTo(map);
        listaMarcadoresMapa.push(marker1);

        //criar lista de resultados de biciletario
        dadosGeograficos.bicicletarios.map((locais)=>{
            listaResultados.appendChild(criarLista(locais));

            //cria marcador dos pontos de bicicletario no mapa
            const marker2 = new mapboxgl.Marker()
                .setLngLat([ locais.lon, locais.lat])
                .addTo(map);
            listaMarcadoresMapa.push(marker2);
             
            const marker3 =new mapboxgl.Marker(criarCaixaDescricao())
                .setLngLat([ locais.lon, locais.lat])
                .addTo(map);
            listaMarcadoresMapa.push(marker3);

        })
        //mostra os resultados e modifica o btn de busca
        secaoResultados.classList.remove("invisible");
        recarregarPagina();

    
    }else{
        alert("Não foi encontrado nenhum ponto de biciletário próximo, para o endereço digitado")
        recarregarPagina();
    }           
}


function recarregarPagina(){
    btn.querySelector('.spinner').classList.add('d-none');
    btn.querySelector('.textoBtn').textContent = 'Buscar';
}



